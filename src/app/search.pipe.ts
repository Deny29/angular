import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'search'
})
// Поиск статей по названию
export class SearchPipe implements PipeTransform {

  transform(users, value) {
    return users.filter(user => {
      return user.name.includes(value)
    })
  }

}
