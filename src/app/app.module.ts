import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HttpModule } from '@angular/http';
import { HoverDirective } from './hover.directive';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';
import { HomePageComponent } from './home-page/home-page.component';
import { SetupPageComponent } from './setup-page/setup-page.component';
import { RouterModule } from '@angular/router';
import { isActivePipe } from './isActive.pipe';
//import {OrderByPipe} from 'fuel-ui/fuel-ui';
import { OrderBy } from './soret.pipe'
import { ValidatorMessageComponent } from './action/validator-message.directive';
import { CustomSelectComponent } from './edit/custom-select/custom-select.component';
//import { EditComponent } from './edit/edit.component';
import { SortUser } from './user/user-sort.component';
import { UnlessDirective } from './unless.directive';
import { UnDirective } from './unless.directive';
import {PaginationComponent} from './pagination/pagination.component';
import { ActionComponent } from './action/action.component';
import { OneActionComponent } from './one-action/one-action.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes = [
  { path: '', component: HomePageComponent },
  { path: 'setup', component: SetupPageComponent },
  { path: 'action', component: OneActionComponent }
  // {path: 'edit', component: EditComponent}
];
//  Импортирование компоненты
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    // EditComponent,
    HoverDirective,
    SearchPipe,
    HomePageComponent,
    SetupPageComponent,
    isActivePipe,
    OrderBy,
    ValidatorMessageComponent,
    CustomSelectComponent,
    SortUser,
    UnlessDirective,
    UnDirective,
    ActionComponent,
    OneActionComponent,
    PaginationComponent

  ],
  //импортированные модули
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes),
    RouterModule,
    ReactiveFormsModule

    //OrderByPipe
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
