import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
// класс обработки наведения мыши
export class HoverDirective {

  @HostBinding('class.ui') isHovered = false;

  @HostListener('mouseenter') onmouseenter() {
    this.isHovered = true;
  }
  @HostListener('mouseleave') onmouseleave() {
    this.isHovered = false;
  }
}
