import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sort-user',
  template: `
    <p>The hero's birthday is {{ birthday | date:format }}</p>
    <button (click)="toggleFormat()">Toggle Format</button>
    <app-user *ngFor="let u of (users | search:searchStr | orderBy1 | active) " [user]="u"  ></app-user>
  `
})
export class SortUser {
/* isMarked = false

  @Input() user;

 ar = '';
  onClick() { 
    
    this.isMarked = true
   
    /*if (this.user = 'true') {

        this.ar = 'Открытая';
      } 
    }*/
  }