import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  /*  template: `<div class="col s3">
   <div class="card" appHover>
   <div class="" [ngClass]="{
     'card blue-grey darken-1': user.active
   }" >
     <div class="card-image">
       <img src="https://zooclub.ru/skat/img.php?w=700&h=700&img=./attach/12000/12669.jpg">
       <span class="card-title" [ngClass]="{'marked': isMarked}">{{ user.name | uppercase }}</span>
     </div>
     <a class="" [ngClass]="{
     'btn-floating halfway-fab waves-effect waves-light red': user.active
   }" ><i class="material-icons" [ngClass]="{
     '': user.active
   }">add</i></a>
     <div class="card-content">
       <p>{{ user.author }}</p>
     </div>
     <div class="card-action">
   <p> Дата создания: {{ user.date | slice:1:19}}</p><!-- Обрезание части даты -->
       <p>
         <input type="checkbox" name="user" id="test6" checked="checked" />
         <label for="test6">Открытая {{ar}} {{user.active}}</label>
 
 
       </p>
       <!--<a (click)="onClick()" class="waves-effect waves-light btn">Открытые</a>
       <a (click)="onClick1()" class="waves-effect waves-light btn">Перейти</a-->
 
       <!--a id="scale-demo" href="#!" class="btn-floating btn-large scale-transition">
     <i class="material-icons">add</i>
   </a-->
     </div>
   </div>
     <div class="collection">
             <a href="#!" class="collection-item"><span class="new badge">4</span>Alan</a>
 
     </div>
 
   </div>
 </div>`,*/
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  isMarked = false

  @Input() user;

  ar = '';
  onClick() {

    this.isMarked = true

    /*if (this.user = 'true') {

        this.ar = 'Открытая';
      } */
  }

}
