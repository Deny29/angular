import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';*/

@Injectable()
export class UsersService {

  size = 8;
  constructor(private http: Http) { }
  //Запрос данных с сервера редома.
  /*return this.http.get('user.json'))*/
  //http://www.json-generator.com/api/json/get/bVZcGERjfS?indent=4
  //'https://randomuser.me/api/?inc=gender,name,picture,location&results=' + this.size + '&nat=gb'
  getUsers() {
    return this.http.get('https://www.json-generator.com/api/json/get/bVZcGERjfS?indent=4')
      .map(function (response) {
        return response.json();
      })
      .map(response => response.articles)
      /* .map(data => data.json().filter(s => s.Phone === 123))
       .subscribe(data => {
         this.studentTEST = data as Students[];
       }, error => console.error(error));*/
      .map(users => {
        return users.map(u => {
          /*return {
            name: u.name.first + ' ' + u.name.last,
            image: u.picture.large,
            geo: u.location.city + ' ' + u.location.state + ' ' + u.location.street
          }*/
          return {
            id: u.idArticle,
            name: u.name,
            image: u.picture,
            date: u.datePublication,
            author: u.author,
            about: u.about,
            active: u.isActive
          }

        })
      })
    /*  .map(data => data.json().filter(s => s.isActive === true))
            .subscribe(data => {
             // this.users = Users as Users[];
            }, error => console.error(error));*/
  }
  /*getUsers() {
      return this.http.request('/app/user.json')
      .map(res => res.json());
    }

    getData(res: Response){

    }*/
  /* constructor(private httpClient: HttpClient) {
   }

   public getDepartmentData(): Observable<any> {
     console.log('getDepartmentData');
     const apiUrl = 'app/french-regions-departments.json';
     return this.httpClient.get(apiUrl);
       // .map((res: any) => {
       //   const data = res.json();
       //   return data;
       // });
   }*/

  /*etUsers(){
      let jsonUrl = '/user.json';
      return this.http.get(jsonUrl)
        .map( (Response)=>Response.json() );
  }*/

  setSize(size) {
    this.size = size;
  }
  users = [
    { name: 'WFM 1' },
    { name: 'WFM 2' },
    { name: 'WFM 3' },
    { name: 'WFM 4' },
    { name: 'WFM 5' },
    { name: 'WFM 6' }
  ]
}
