import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({ selector: '[appUnless]'})
export class UnlessDirective {
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set appUnless(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}

@Directive({ selector: '[appUnles]'})
export class UnDirective {
  private hasView1 = false;

  constructor(
    private templateRef1: TemplateRef<any>,
    private viewContainer1: ViewContainerRef) { }

  @Input() set appUnles(cond: boolean) {
    if (!cond && !this.hasView1) {
      this.viewContainer1.createEmbeddedView(this.templateRef1);
      this.hasView1 = true;
    } else if (cond && this.hasView1) {
      this.viewContainer1.clear();
      this.hasView1 = false;
    }
  }
}

